<title>MSC De Pijl</title>
<base href="https://de-pijl.be/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/html5.js" type="text/javascript"></script>
<link rel="icon" type="image/png" href="images/favicons/favicon.png">
<link rel="stylesheet" href="css/font.css">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/stylesheet.css">
