<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>NMBS kleuren</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">NMBS kleuren</h1>
    <p>De NMBS kleuren in model zijn soms erg moeilijk te vinden. Dankzij NMBS-kenner Jean M. hebben we onderstaande tabel samengesteld met de meest gangbare NMBS kleuren en hun overeenkomstige kleurcodes van Revell, Humbrol, Jocadis en andere merken. Sommige potjes zijn nog verkrijgbaar, anderen niet.</p>

    <h2 class="w3-xxxlarge w3-text-green">Opmerkingen bij deze tabel</h2>

    <p>Jean bemerkt dat de door hem opgegeven RALnummers de tinten die bij benadering overeenkomen met de kleuren die door de NMBS worden gebruikt. Waar het mogelijk is heeft Jean telkens een verftint gezocht die overeenkomt met de opgegeven RALnummer. Vroeger vermeldde Revell de RAL-code op zijn potjes, maar dat is tegenwoordig ook niet meer het geval.</p>

    <p>Humbrol verwijst op een folder naar de overeenkomende RALkleuren. Helaas gaat dit slechts op voor een beperkt aantal potjes uit hun assortiment.</p>

    <p>Opgepast, wanneer je de opgegeven RALkleur uit de gepubliceerde lijst gaat opzoeken op een uitgebreide RALkaart, dan lijken deze kleuren op het eerste zicht helemaal niet op de bestaande NMBS kleuren. Dit is bijvoorbeeld duidelijk het geval voor RAL 1017 dat moet doorgaan voor Benelux-geel. <b>Vergelijken en uittesten is steeds aangewezen</b> vooraleer je met de kwast lustig je model begint te bewerken.</p>

    <p>Ook de wijze waarop je zelf de kleuren van het NMBS-materiaal ervaart zal in sterke mate je eigen voorkeur voor een bepaalde tint bepalen. Zo kiest Jean zelf steeds voor een donkerder geel dan sommige gebruikt wordt. Het kanariegeel van sommige diesellocomotieven vind Jean verschrikkelijk. Vandaar dat je voor het geel zoveel mogelijkheden aangeboden krijgt.</p>

    <p>De meeste verven die Jean aanraadt zijn van de bekende merken zoals Humbrol of Revell, je kan ze in de meeste hobbyzaken vinden. Van Revell waren enkele jaren geleden de meeste potjes nog voorzien van een RAL-nummer. Nu staan ze er niet meer op. Bovendien zijn <b>sommige nummers van Humbrol of Revell zijn niet meer verkrijgbaar</b>.</p>

    <p>Minder gekend, maar daarom niet minder geschikt, zijn de <b>Tamiya acrylverven en Model Master</b>. Ze bieden het voordeel dat ze <b>snel drogen</b>, waardoor na 10 minuten de verf zo goed als handdroog is. Tamiya verdunnen doe je best met isopropanol, een product dat je bij elke apotheker kunt krijgen. Van Tamiya gebruikt Jean vaak het zwart.</p>

    <p>Voor het elektrisch blauw heeft Jean geen enkele gelijkende verf gevonden. Als er iemand een goede suggestie heeft, horen we dat graag! Turkoois blauw (RAL 5018) lijkt Jean te licht en kan best donkerder gemaakt worden met bijvoorbeeld groenblauw (RAL 5001). Gespecialiseerde verfzaken kunnen de verf bij benadering maken op basis van een staal en in potjes van doorgaans minimum 1 liter. Een goede staal kun je alleen maken op basis van een lik uit de grote pot van de spoorwegen.</p>

    <p>Jocadis heeft intussen verschillende basiskleuren, helaas bestaat <b>Jocadis ondertussen niet meer</b>. Soms kom je de verfjes nog tegen op beurzen en 2dehandsmarkten.</p>
    
    <h2 class="w3-xxxlarge w3-text-green">Enkele tips</h2>

    <p>Gele strepen voor de eerste klasse rijtuigen of voor sommige locomotieven moeten niet noodzakelijk geschilderd worden, maar kunnen evengoed aangebracht worden met watertransfer van Micro Scale Decals. Dit zijn grote vellen gekleurde watertransfer van 12,5 X 20 cm. Met een scherp hobbymes en een metalen latje van 20 cm kan je de gewenste strookjes uit het vel snijden. 

    <p>Als hobbymes gebruikt Jean steeds een nieuw X-Acto mes of een pedicuremesje nr. 11 en/of 15.</p>

    <p>Transparante kleuren zijn weinig dekkend en daarom ook moeilijk aan te brengen. Cadmiumgeel is zo een kleur. Om dit probleem te omzeilen kan je eerst een dekkend geel of gewoon wit spuiten. Daar bovenop breng je een laag van de transparante kleur aan.</p>

    <p>Hetzelfde probleem kun je hebben met rood. Voeg een beetje dekkend geel bij het rood zodat het transparant effect gebroken wordt. De eindafwerking doe je opnieuw met de originele kleur. Trouwens rood komt veelal beter tot zijn recht wanneer je enkele druppels geel toevoegt. Maar dat is een persoonlijk smaak.</p>

    <p>Wanneer je een model afwerkt met een satijnvernis, of mat/satijn, dan worden de ruitjes automatisch in dezelfde vernis gespoten. Deze ruitjes kan je nadien opnieuw mooi doorschijnend afwerken door de ruitjes met een penseel met een glanzende vernis een nieuw laagje te geven. Zelf gebruikt Jean hiervoor Tamiya clear X-22 </p>
    
    <p>In geen geval kan Jean of MSC De Pijl aansprakelijk gesteld voor miskleunen als gevolg van fouten in het artikel en het verkeerd gebruik van verf. Aanvullingen zijn steeds van harte welkom.</p>

    <h2 class="w3-xxxlarge w3-text-green">Hulpmiddelen</h2>
    <table class="w3-table-all">
      <tr>
        <th>Type</th>
        <th>Fabrikanten</th>
      </tr>
      <tr>
        <td>Verdunningsmiddelen</td>
        <td>Humbrol en Revell thinner voor de meeste enamel verven of lakverven. Isopropanol voor Tamiya verven. Speciale thinner voor Model Master verven.</td>
      </tr>
      <tr>
        <td>Grondverf</td>
        <td>Humbrol zinkchromaat, Humbrol 1, Trimetal: plomoferrine antiroest rode menie</td>
      </tr>
      <tr>
        <td>Vernis glanzend, satijn of mat</td>
        <td>Elke vernis verkrijgbaar in de kleinhandel.<br>Bijvoorbeeld: Humbrol mat 49 of glanzend 35, Revell glanzend 1 of mat 2, Tamiya glazend X22 of mat X21 (minder bruikbaar)</td>
      </tr>
    </table>

    <h2 class="w3-xxxlarge w3-text-green">Kleuren</h2>
    <table class="w3-table-all">
      <tr>
        <th>NMBS kleur</th>
        <th>Gebruikt bij</th>
        <th>Fabrikanten</th>
      </tr>
      <tr>
        <td>Hedendaags groen of flessengroen</td>
        <td>Diesellocomotieven, stoomlocomotieven en goederenwagons</td>
        <td>Levislux groen, Jocadis 99000 (voorkeur), Tamia X5 (bij benadering)</td>
      </tr>
      <tr>
        <td>Lichtgroen (RAL 6029)</td>
        <td>Tweekleurenperiode</td>
        <td>Humbrol 2, Revell 61, Model Master 1524, Jocadis 99004 of Tamiya X28</td>
      </tr>
      <tr>
        <td>Donkergroen (RAL 6005)</td>
        <td>Tweekleurenperiode en goederenwagons</td>
        <td>Humbrol 3, Revell 62, Jocadis 99009, Model Mater 2716 of Tamiya X28 (bij benadering)</td>
      </tr>
      <tr>
        <td>Geel (RAL 1021) of Cadmiumgeel</td>
        <td>Elektrische locomotieven en diesellocomotieven</td>
        <td>Humbrol 99, Jocadis 99007, Tamiya XF-8 (bij benadering)</td>
      </tr>
      <tr>
        <td>Beneluxgeel</td>
        <td>Oud Beneluxgeel - Hondekop</td>
        <td>Benaderen via: Humbrol 24 + 69 (1:1), Jocadis 99002 of Model Master 1569 + 1514</td>
      </tr>
      <tr>
        <td>Chromaatgeel (RAL 1018 bij benadering)</td>
        <td>Reeks 25.5 (Benelux)</td>
        <td>Humbrol 69 of benaderen via Revell 12, Jocadis 99002 of Model Master 1514</td>
      </tr>
      <tr>
        <td>Geel</td>
        <td>Motorwagens</td>
        <td>Jocadis 99002</td>
      </tr>
      <tr>
        <td>Geel (RAL 1026)</td>
        <td></td>
        <td>Humbrol 194, Revell 312 of Gunze Sangyo 97</td>
      </tr>
      <tr>
        <td>Elektrisch blauw (RAL 5018) of turkoois</td>
        <td>Reeks 15 en 16</td>
        <td>Humbrol 14 + 101 (1:10, experimenteel), Revell 52 + 364 (1:10, experimenteel) of Jocadis 99006 (bij benadering)</td>
      </tr>
      <tr>
        <td>Blauw (RAL 5011)</td>
        <td>Elektrische locomotieven</td>
        <td>Jocadis 99006 of benaderen via: Humbrol 48 + 14 (1:1) of Revel 51 + 52 (1:1)</td>
      </tr>
      <tr>
        <td>Blauw (RAL 5013) of kobaltblauw (Oxford Blue)</td>
        <td>Reeks 25.5, Vauban en Railtour</td>
        <td>Humbrol 68/198, Revell 53/350, Gunze Sngyo 39 of Tamiya X16</td>
      </tr>
      <tr>
        <td>Bordeaux (RAL 3004), purperrood, crimsonrood</td>
        <td>AM75 (vierledig), AM80 (Break), AM86 (Duikersbril) en rijtuigen M2 &amp; M4</td>
        <td>Humbrol 20, Revell 32/331, Jocadis 99001 of Model Master 1501</td>
      </tr>
      <tr>
        <td>Orange (RAL 2004) or puur oranje</td>
        <td>I6 &amp; I10 rijtuigen en de reeks 800 (Grijze Muis)</td>
        <td>Humbrol 18, Revell 30, Model Master 1527 of Tamiya X6</td>
      </tr>
      <tr>
        <td>Grijs (RAL 7003) of mosgrijs (Army Green)</td>
        <td>Reeks 800 (Grijze Muis)</td>
        <td>Humbrol 86, Revell 45 of Tamiya XF14</td>
      </tr>
      <tr>
        <td>Lichtgrijs (RAL 7035)</td>
        <td>Vauban</td>
        <td>Humbrol 196, Revell 371 of benaderen via: Model Master 1731 + 1768 of Tamiya XF20 + XF2</td>
      </tr>
      <tr>
        <td>Donkergrijs (RAL 7037) of stofgrijs</td>
        <td>Vauban (dak en schorten) en reeks 27</td>
        <td>Humbrol 5 of Tamiya XF66 (bij benadering)</td>
      </tr>
      <tr>
        <td>Donkergrijs (RAL 7022)</td>
        <td>Vauban (draaistellen)</td>
        <td>Benaderen via: Humbrol 85/184, Revell 9, Model Master 1592 of Tamiya X18</td>
      </tr>
      <tr>
        <td>Oxyderood (RAL 3009)</td>
        <td>Goederenwagons (rood)</td>
        <td>Humbrol 70 of Revell 37</td>
      </tr>
      <tr>
        <td>Bruinrood (RAL 3011)</td>
        <td>Goederenwagons (rood)</td>
        <td>Humbrol 113 + 70, Revell 83 + 37, Model Master 1785, Model Master 2009 + 2007 (1:1) of Model Master 5130</td>
      </tr>
      <tr>
        <td>Rood (RAL 3020), signaalrood of verkeersrood</td>
        <td>Vauban</td>
        <td>Humbrol 19/174, Revell 31, Model Master 2724, 1503 of 1590</td>
      </tr>
      <tr>
        <td>Rood</td>
        <td>Vauban</td>
        <td>Humbrol 19/174, Revell 31, Model Master 2724, 1503 of 1590</td>
      </tr>
      <tr>
        <td>Zwart (RAL 9011) of grafietzwart</td>
        <td></td>
        <td>Humbrol 33, Revell 8, Model Master 1794 of Tamiya XF1</td>
      </tr>
      <tr>
        <td>Kolenzwart (RAL 9005)</td>
        <td></td>
        <td>Humbrol 21, Revell 7, Model Master 1547 of Tamiya X1</td>
      </tr>
      <tr>
        <td>Diepzwart (RAL 9005)</td>
        <td>Stoomlocomotieven</td>
        <td>Humbrol 21, Revell 7, Model Master 1747 of Tamiya X1</td>
      </tr>
      <tr>
        <td>Wit (RAL 9001)</td>
        <td></td>
        <td>Humbrol 34, Revell 5, Model Master 1768 of Tamiya XF2</td>
      </tr>
      <tr>
        <td>Bruin (RAL 8017) of chocoladebruin</td>
        <td>Stoomlocomotieven (bruin)</td>
        <td>Humbrol 10, Revell 81, Jocadis 99008, Model Master 1540 of Trimetal donkerbruin 202 (beste keuze)</td>
      </tr>
      <tr>
        <td>Groen NMVB</td>
        <td>NMVB buurtspoorwegen</td>
        <td>Jocadis 99020</td>
      </tr>
      <tr>
        <td>Grijs NMVB</td>
        <td>NMVB buurtspoorwegen</td>
        <td>Jocadis 99022 (grijs)</td>
      </tr>
      <tr>
        <td>Crem&egrave;geel NMVB</td>
        <td>Trams (geel) en NMVB buurtspoorwegen</td>
        <td>Humbrol 103 of Jocadis 99021</td>
      </tr>
      <tr>
        <td>Zilver</td>
        <td></td>
        <td>Humbrol 11, Revell 90, Model Master 1546, 1780, 2734 of Tamiya X11</td>
      </tr>
      <tr>
        <td>Goud</td>
        <td></td>
        <td>Humbrol 16, Revell 94, Model Master 2722, 1744 of Tamiya X12</td>
      </tr>
      <tr>
        <td>Koper</td>
        <td></td>
        <td>Humbrol 12, Revell 93, Model Master 1551 of Tamiya XF6</td>
      </tr>
      <tr>
        <td>Brons</td>
        <td></td>
        <td>Humbrol 55 of Revell 95</td>
      </tr>
      <tr>
        <td>Brass</td>
        <td></td>
        <td>Humbrol 54, Revell 92, Model Master 1582, 1782 of Tamiya XF6</td>
      </tr>
    </table>
    
<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
