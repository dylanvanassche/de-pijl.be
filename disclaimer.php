<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Disclaimer</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Home -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Disclaimer</h1>
    <h1 class="w3-xxxlarge w3-text-green">Overeenkomst</h1>
    <hr class="w3-round">
    <p class="bold">MSCM De Pijl vzw</p>
    <p>MSC De Pijl vzw, hierna De Pijl genoemd, is een vereniging met als doel het promoten van de minatuurspoorhobby in al haar facetten met inbegrip haar belangstelling voor het grootbedrijf. (BS 28/09/2004)</p>
    <p class="bold">Aansprakelijkheid van De Pijl</p>
    <p>De Pijl besteedt veel aandacht en zorg aan haar website en streeft ernaar dat alle informatie zo volledig, juist, begrijpelijk, nauwkeurig en actueel mogelijk is. Ondanks alle voortdurend geleverde inspanningen kan De Pijl niet garanderen dat de ter beschikking gestelde informatie volledig, juist, nauwkeurig of bijgewerkt is. Als de op (of via) de site verstrekte informatie tekortkomingen vertoont, zal De Pijl de grootst mogelijke inspanning leveren om dat zo snel mogelijk te corrigeren. Als u onjuistheden vaststelt, kunt u contact opnemen met de beheerder van de site via de &#0147;<a href="index.html#contact">Contact</a>&#0148; sectie. De Pijl kan niet aansprakelijk gesteld worden voor mogelijke fouten die op de website zouden voorkomen. Ze geven u in geen enkel geval recht op enige financiële compensatie.</p>
    <p>Alle commentaar of materiaal dat u ongevraagd en uit eigen beweging aan de De Pijl bezorgt, met inbegrip van de gegevens of reacties betreffende de inhoud van deze site, zal als niet-vertrouwelijk worden   beschouwd. De Pijl wijst iedere verplichting van de hand inzake het gevolg dat aan die algemene reacties en gegevens moet worden gegeven. De Pijl mag die reacties en gegevens naar eigen goeddunken gebruiken, behoudens beperkingen bepaald in de wet van 8 december 1992 tot bescherming van de persoonlijke levenssfeer ten opzichte van de verwerking van persoonsgegevens. U stemt ermee in dat De Pijl de ideeën, concepten, kennis en technieken, vervat in uw reacties, kan aanwenden voor welk doel ook, met inbegrip van, onder andere, de ontwikkeling en het concipiëren van nieuwe ideeën op haar website.</p>
    <p class="bold">Links naar websites beheerd door derden</p>
    <p>De website De Pijl bevatten links naar websites die beheerd worden door derden. Deze links zijn uitsluitend ter informatie van de gebruiker aangebracht. De Pijl controleert deze websites en de informatie die zich dan erop bevindt niet. De Pijl kan niet verantwoordelijk gesteld worden voor de inhoud of de kwaliteit van deze websites. Een link houdt niet noodzakelijk in dat De Pijl en de uitbaters van deze websites op de één of andere manier samenwerken of dat De Pijl de informatie op deze websites goedgekeurd heeft. Vanzelfsprekend is De Pijl niet verantwoordelijk voor eventuele schendingen van de bescherming van de persoonlijke levenssfeer door derden waarnaar wordt verwezen.</p>
    <p class="bold">Links naar de website De Pijl</p>
    <p>Natuurlijk mag je op je eigen website een link aanbrengen naar de startpagina van de website van De Pijl. We appreciëren het als je ons hiervoor op voorhand contacteert via de &#0147;<a href="index.html#contact">Contact</a>&#0148; sectie. </p>
    <p class="bold">Gebruik van teksten</p>
    <p>De teksten op de website De Pijl zijn vrij kopieerbaar mits bronvermelding naar De Pijl en desgevallend ook naar de vermelde auteur. Het gebruik ervan is volledig vrij, op voorwaarde dat je de bron vermeldt. Als je bij het citeren wijzigingen in de teksten aanbrengt, moet je dit eveneens vermelden. (zie ook wet van 30 juni 1994   betreffende het auteursrecht en de naburige rechten).</p>
    <p class="bold">Geschillen</p>
    <p>Deze on-lineovereenkomst in deze disclaimer en alle geschillen en vorderingen die voortkomen uit het gebruik van deze site of enig gegeven dat erop staat, vallen onder de toepassing van het Belgisch recht. Raadpleging van deze website houdt in dat u zich onderwerpt aan de rechtspraak van de rechtbanken van Mechelen, België, en dat u aanvaardt om alle gedingen alleen voor die rechtbanken te brengen.</p>

    <h1 class="w3-xxxlarge w3-text-green" id="privacyverklaring">Privacyverklaring</h1>
    <p class="bold">Conform de Algemene Verordening Gegevensbescherming (AVG) / General Data Protection Regulation GDPR</p>
    <p>MSC De Pijl vzw bewaart enkel de persoonsgegevens die ze ontvangt bij het afsluiten van je lidmaatschap. (naam, adres, geboortedatum, contactgegevens als je deze doorgaf). Tijdelijk bewaren we gegevens van bevriende personen of relaties, die nuttig zijn voor onze werking, en die als genodigden kunnen gezien worden.</p>
    <p>Deze gegevens worden opgeslagen in de beveiligde database van de vzw. Die is uitsluitend toegankelijk voor de raad van bestuur. Zij dient ten allen tijde deze gegevens met de nodige discretie te behandelen. </p>
    <p>In geen enkel geval kunnen deze gegevens aan derden worden doorgegeven, zonder voorafgaandelijke uitdrukkelijke toestemming van de betrokkene. Alle personen die namens de vzw van je gegevens kennis kunnen nemen, zijn gehouden aan geheimhouding.</p>
    <p>De gegevens worden gebruikt voor:
        <ul>
            <li>de ledenadministratie</li>
            <li>het bezorgen van het ledenblad, uitnodigingen voor en deelname aan de activiteiten, zo nodig nieuwsbrief of dringende berichten</li>
            <li>de wettelijk voorziene formaliteiten in het kader van de VZW-wetgeving</li>
            <li>de verzekeringen</li>
        </ul>
    </p>
    <p>Je hebt het recht deze gegevens in te zien of te verbeteren, na vraag via de &#0147;<a href="index.html#contact">Contact</a>&#0148; sectie. Na ontslag als lid wordt enkel de wettelijk voorziene info bijgehouden.</p>
    <p>Deelname aan de activiteiten van de vereniging brengt mee dat hiervan verslagen en openbaar beeldmateriaal kan worden gemaakt en gepubliceerd.</p>
    <p>Het aanvaarden van het lidmaatschap, brengt mee dat de leden met onze privacy-politiek instemmen.</p>
    <p>Goedgekeurd op de bestuursvergadering van 22/5/2018 door het bestuur van MSC De Pijl.</p>

    <h1 class="w3-xxxlarge w3-text-green">Cookies</h1>
    <hr class="w3-round">
    <p>De website van MSC De Pijl maakt gebruikt van cookies voor het bijhouden van statistieken over het aantal bezoekers en andere zaken. Door gebruik te maken van deze website staat u toe dat deze website cookies gebruikt bij uw bezoek.</p>
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
