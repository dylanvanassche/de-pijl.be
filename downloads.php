<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Downloads</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Downloads</h1>
    <h1 class="w3-xxxlarge w3-text-green">Clubverband</h1>
    <hr class="w3-round">
    <ul>
      <li><a href=downloads/kroniek.pdf target="_blank">Kroniek MSC De Pijl (1974 - 2016)</a></li>
      <!--<li><a href=downloads/verhuis.pdf target="_blank">De verhuis van Mechelen naar Vilvoorde</a></li>
      <li><a href=downloads/opbouwLokaal.pdf target="_blank">De opbouw van het nieuwe lokaal</a></li>
      <li><a href=downloads/tips.pdf target="_blank">Modelbouwtips</a></li>-->
    </ul>

    <h1 class="w3-xxxlarge w3-text-green">Archief</h1>
    <hr class="w3-round">
    <p>De &#0147;Gazet&#0148; was het vroegere clubblad van MSC De Pijl, hieronder kunnen de uitgaves worden geraadpleegd uit het digitale archief. Eind 2009 is MSC De Pijl gestopt met het publiceren van de Gazet.</p>
    <ul>
      <li><a href=downloads/gazet154.pdf target="_blank">Gazet 154</a></li>
      <li><a href=downloads/gazet155.pdf target="_blank">Gazet 155</a></li>
      <li><a href=downloads/gazet158.pdf target="_blank">Gazet 158</a></li>
      <li><a href=downloads/gazet159.pdf target="_blank">Gazet 159</a></li>
      <li><a href=downloads/gazet160.pdf target="_blank">Gazet 160</a></li>
      <li><a href=downloads/gazet161.pdf target="_blank">Gazet 161</a></li>
      <li><a href=downloads/gazet162.pdf target="_blank">Gazet 162</a></li>
      <li><a href=downloads/gazet163.pdf target="_blank">Gazet 163</a></li>
      <li><a href=downloads/gazet164.pdf target="_blank">Gazet 164</a></li>
      <li><a href=downloads/gazet165.pdf target="_blank">Gazet 165</a></li>
      <li><a href=downloads/gazet166.pdf target="_blank">Gazet 166</a></li>
      <li><a href=downloads/gazet167.pdf target="_blank">Gazet 167</a></li>
      <li><a href=downloads/gazet168.pdf target="_blank">Gazet 168</a></li>
      <li><a href=downloads/gazet169.pdf target="_blank">Gazet 169</a></li>
      <li><a href=downloads/gazet170.pdf target="_blank">Gazet 170</a></li>
      <li><a href=downloads/gazet171.pdf target="_blank">Gazet 171</a></li>
      <li><a href=downloads/gazet172.pdf target="_blank">Gazet 172</a></li>
      <li><a href=downloads/gazet173.pdf target="_blank">Gazet 173</a></li>
      <li><a href=downloads/gazet174.pdf target="_blank">Gazet 174</a></li>
      <li><a href=downloads/gazet175.pdf target="_blank">Gazet 175</a></li>
    </ul>
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
