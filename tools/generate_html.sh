#!/bin/bash

echo "Generating HTML pages out of PHP"
cd ..
files=( index modulebaan marklinbaan bibliotheek clubbaan downloads disclaimer 404 )
for i in "${files[@]}"
do
	echo $i
        php -f "$i.php" > "$i.html"
        sed -i "s/.php/.html/g" "$i.html"
done

echo "Creating 'public' folder for Gitlab Pages"
rm -r public
mkdir -p public

echo "Copying all HTML pages and assets to folder 'public'"
mv *.html public
cp -r css public/css
cp -r js public/js
cp -r images public/images
cp -r downloads public/downloads
