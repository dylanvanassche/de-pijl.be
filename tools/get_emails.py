#!/bin/python

def main(path):
    previous_line = ""
    address_counter = 0
    email_addresses = []
    output_file = "out-{}".format(path.replace("html", "txt"))
    with open(path) as f:
        for line in f:
            if "@" in line and "Forward address" in previous_line:
                address_counter += 1
                a = line.strip()
                email_addresses.append(a + "\n")
                print(a)

            previous_line = line

    with open(output_file, "w") as f:
        f.writelines(email_addresses)

    print("\nAddresses: {}".format(address_counter))
    print("File: {}".format(path))
    print("Output: {}".format(output_file))
    print("-" * 50)

if __name__ == "__main__":
    main("leden2012.html")
    main("leden2016.html")
