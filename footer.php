<footer class="w3-light-grey w3-container w3-padding-32">
  <p class="footer-content w3-center">
    Copyright &copy; MSC De Pijl (2019) - <a href="disclaimer.php">Disclaimer</a><br>
  </p>
</footer>
