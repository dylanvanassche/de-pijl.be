<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Clubbaan</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">
  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">De oude clubbaan</h1>
    <h1 class="w3-xxxlarge w3-text-green">Het verhaal achter...</h1>
    <hr class="w3-round">
    <p>In 1976 werd gestart met een grote clubbaan.</p> 
    <p>Het werd een U-vormigs ontwerp met drie zichtbare stations en 3 verdoken opstelstations.  Vanuit centraal station kon je in kring 1 naar links om via een kleine carrousel te klimmen naar “Doorgang”. Hier kon je over twee bruggetjes met zicht op  het lager liggend depot van Centraal station ofwel naar het opstelstation met de naam “Heist onder den berg” ofwel  via de “Paradestrecke” naar het opstelstation van de zogenoemde “Benedenbaan”. In dit station waren twee keerlussen voorzien. Dit verdoken station kreeg de naam “Lievekenshoek” omwille van de enkele ontluikende liefdes. Van hieruit kon je terug naar het Centraal station.</p>
    <p>Via het Centraal Station kon je weer naar links naar kring 2 de “Bovenbaan” genoemd. Een grote carrousel overwon het hoogteverschil en dan ging de rit over de ”Bietchtahlbrug” Deze brug was in miniatuur van in het begin dubbelsporig en recht ook al ligt de echte brug in Zwitserland in een lichte bocht en was deze oorspronkelijk enkelsporig. We reden verder naar de “Sint Lucasbrug”. Wie hier rechts uit de trein keek zag onderaan de Paradestrecke en het nog lager liggend Centraal station. We kwamen aan in het hoogst gelegen station “Haidikon”  Dit was Zwitsers geïnspireerd en had drie doorgaande sporen en twee kopsporen.</p>
    <p>Via het mooie in een S-bocht gelegen viaduct en een lange afdaling met carrousel keerden we terug naar Het Centraal station.</p>
    <p>Beide kringen waren oorspronkelijk dubbelsporig aangelegd. Later werd het traject Haidikon- viaduct-carrousel- Centraal station enkelsporig. Dit zorgde voor meer spelmogelijkheden.</p>
    <p>Het geheel was voorzien van een bloksysteem met Belgische seinen, eerst analoog dan digitaal met Zimo als besturingssysteem.</p>
    <p>Opvallend aan dit concept was dat de bezoeker nooit wist waar de treinen opnieuw te voorschijn kwamen.</p>
    <p>Jammer genoeg hebben we dit unieke, ja zelfs internationaal gekend pronkstuk moeten afbreken in november 2015.</p>

    <!-- Foto's -->
    <h1 class="w3-xxxlarge w3-text-green">Foto's</h1>
    <hr class="w3-round">
    <noscript>
      <div class="w3-panel w3-red w3-center">
      <h3>Javascript is uitgeschakeld!</h3>
      <p>Schakel Javascript in om deze foto's te vergroten.</p>
    </div>
    </noscript>

    <div id="modal" class="w3-modal w3-center zoom-out" onclick="this.style.display='none'">
      <img class="w3-modal-content w3-round modal_responsive" id="modal_img">
    </div>

    <script type="text/javascript" language="javascript" src="js/modal.js"></script>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan4.jpg" alt="Foto 4" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan5.jpg" alt="Foto 5" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan6.jpg" alt="Foto 6" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan7.jpg" alt="Foto 7" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan8.jpg" alt="Foto 8" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan9.jpg" alt="Foto 9" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan10.jpg" alt="Foto 10" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan11.jpg" alt="Foto 11" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/clubbaan12.jpg" alt="Foto 12" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
