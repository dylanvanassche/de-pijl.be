<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Bibliotheek</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Bibliotheek</h1>
    <h1 class="w3-xxxlarge w3-text-green">Geaboneerde magazines</h1>
    <hr class="w3-round">
    <p class="bold">Modelspoor<p/>
    <ul>
      <li><a href="http://www.modelspoormagazine.be/" target="_blank">Modelspoormagazine (BE)</a></li>
      <li><a href="http://trains.lrpresse.com/CT-220-loco-revue.aspx/" target="_blank">Loco-Revue (FR)</a></li>
    </ul>
    <p class="bold">Grootbedrijf<p/>
    <ul>
      <li><a href="http://www.pfttsp.be/index.php/nl-NL/publicaties/op-de-baan/201-op-de-baan/" target="_blank">Op de baan (BE)</a></li>
    </ul>

    <h1 class="w3-xxxlarge w3-text-green">Archief</h1>
    <hr class="w3-round">
    <p>Door de jaren heen heeft MSC De Pijl een uitgebreid archief opgebouwd dat je kan raadplegen in de gezellige bar van MSC De Pijl bij een hapje en een drankje. <br>Verder beschikt de club over een archief beeldmateriaal dat je ter plekke ook kan bekijken.</p>
    <p class="bold">Ingebonden magazines<p/>
    <ul>
      <li>Eisenbahn Journal (1986 - 2008) <span class="italic">Duitsland</span></li>
      <li>Eisenbahn Kurier (1982 - 2009) <span class="italic">Duitsland</span></li>
      <li>Eisenbahn Magazine (1972 - 2009) <span class="italic">Duitsland</span></li>
      <li>Model Railway Journal <span class="italic">UK</span></li>
      <li>Railhobby (1980 - 2008) <span class="italic">Nederland</span></li>
      <li>Model Railroader (1972 - 1999) <span class="italic">USA</span></li>
      <li>MIBA (1977 - 2011) <span class="italic">Duitsland</span></li>
      <li>Vois Ferrees <span class="italic">Frankrijk</span></li>
      <li>Eisenbahn Magazine (1986 - 2008) <span class="italic">Duitsland</span></li>
      <li>En nog andere ingebonden magazines van diverse aard...</li>
    </ul>
    <p class="bold">Boeken<p/>
    <ul>
      <li>Grootbedrijf
        <ul>
          <li>Algemeen (24 items)</li>
          <li>Afrika (2 items)</li>
          <li>Oostenrijk (1 item)</li>
          <li>Azië (3 items)</li>
          <li>België (61 items)</li>
          <li>Zwitserland (15 items)</li>
          <li>Duitsland (21 items)</li>
          <li>Europa (16 items)</li>
          <li>Frankrijk (6 items)</li>
          <li>Groot-Hertogdom Luxemburg (3 items)</li>
          <li>Nederland (3 items)</li>
          <li>Verenigd Koninkrijk (11 items)</li>
          <li>USA (2 items)</li>
          <li>Trams (32 items)</li>
        </ul>
      </li>

      <li>Modelbouw
        <ul>
          <li>Elektronica &amp; elektriciteit (22 items)</li>
          <li>Algemene modelbouw (41 items)</li>
          <li>M&auml;rklin (11 items)</li>
        </ul>
      </li>

      <li>Onderdelencatalogi
        <ul>
          <li>M&auml;rklin</li>
          <li>Fleischmann</li>
          <li>Roco</li>
          <li>LIMA</li>
        </ul>
      </li>
    </ul>

    <p class="bold">Beeldmateriaal: DVD's en video's<p/>
    <ul>
      <li>Groot bedrijf: Duitsland, Japan, USA, Zwitserland, Frankrijk, ...</li>
      <li>Modelbouw: instructievideo's van MIBA (DIY)</li>
    </ul>

    <p class="bold">Gazet<p/>
    <p>De &#0147;Gazet&#0148; was het vroegere clubblad van MSC De Pijl, de vorige edities die enkel in papieren versie zijn uitgebracht kunnen geraadpleegd worden in de bibliotheek van MSC De Pijl. Een gedeelte hiervan is ook in digitale vorm beschikbaar onder de rubriek &#0147;<a href=downloads.html>Downloads</a>&#0148;.</p>

  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
