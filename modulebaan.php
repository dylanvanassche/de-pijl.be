<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Modulebaan</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Foto's -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Modulebaan</h1>
    <noscript>
      <div class="w3-panel w3-red w3-center">
        <h3>Javascript is uitgeschakeld!</h3>
        <p>Schakel Javascript in om deze foto's te vergroten.</p>
      </div>
    </noscript>
    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan1.jpg" alt="Foto 1" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan2.jpg" alt="Foto 2" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan3.jpg" alt="Foto 3" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>
  </div>

  <div id="modal" class="w3-modal w3-center zoom-out" onclick="this.style.display='none'">
    <img class="w3-modal-content w3-round modal_responsive" id="modal_img">
  </div>

  <script type="text/javascript" language="javascript" src="js/modal.js"></script>

  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-xxxlarge w3-text-green">Het ontstaan</h1>
    <hr class="w3-round">
    <p>In 1988 waren er in de vereniging een aantal mensen die besloten om samen een modulebaan te bouwen. Om het warm water niet opnieuw te moeten uitvinden hebben we eerst rondgekeken wat er in de ons omringende landen al gemaakt / gekend was. Op basis van deze verkenningsronde werd  dan een idee / plan uitgewerkt en werd de eenheidsnorm van de bakken bepaald, ligging van het spoor t.o.v. de rand, de hoogte van de baan, de aan te houden elektrische standaard… vastgelegd.</p>
    <p>Om als één baan over te komen bij het publiek werd besloten om rond een vast thema te werken, dezelfde ballast samenstelling te gebruiken, zelfde strooimateriaal, enz om aldus een homogeen en samenhangend geheel te bekomen. Als thema werd besloten lijn 42, Luik->Gouvy->Luxemburg, als inspiratiebron te nemen. Redenen waren : niet geëlektrificeerd, m.a.w. uitsluitend diesel tractie, lokale- en internationale reizigerstreinen, afwisselend goederen verkeer, interessant reliëf…</p>
    <p>Het was niet de bedoeling om een exacte kopie te maken van deze lijn, dat is onmogelijk, maar om een aantal herkenbare punten erin te zetten, die dan door desbetreffende leden, die tevens ook eigenaar zijn van die modules, gebouwd werden en worden. Daarbij zijn o.a. de stations van Vielsalm en Trois Ponts.</p>
    <p>Al snel werd er besloten om deze baan digitaal uit te voeren. Begin jaren 90 was dat geen evidente keuze. Er werd destijds gekozen voor het systeem van Zimo. Dat is tot op vandaag up to date gehouden d.m.v. upgrades en werkt tot ieders tevredenheid.</p>
    <p>Met deze baan, in grote en kleinere samenstelling en in verschillende stadia van afwerking, is de vereniging meerdere malen ‘op de boer’ gegaan. Dat was o.a. naar MOMA Brussel (jaren 90-tig), Leiden Nederland, Maubeuge Frankrijk, Modelspoor Expo in Mechelen (2004 – 2006), Dortmund Duitsland (2008), Euromodelbouw Hasselt (2011). Die laatste was de grootste samenstelling die we ooit hebben rechtgezet. We hadden hiervoor een oplegger van 10m nodig om alles erin te krijgen…</p>
    <p>In onze huidige lokalen in het Tuchthuis te Vilvoorde is een beperkt deel van deze baan opgesteld en moet toelaten om 2-rail rijders ook rijplezier te kunnen aanbieden. De andere modules staan elders in opslag wegens geen plaats in Vilvoorde.</p>
    <p>Er worden enkele aangepaste modules gebouwd om o.a. een ‘landmerk’ van onze voormalige clubbaan, de Bietschtahl brug, te kunnen plaatsen in de modulaire baan. Tevens zullen er 2 ophaalbruggen gebouwd worden om ons niet meer hoeven te bukken om aan de andere kant van de baan te geraken.</p>
    <p>Bent u geïnteresseerd in deze baan, aarzel dan niet om eens langs te komen op onze wekelijkse clubavonden. Meer informatie hierover vind u <a href=index.html#locatie>hier</a>.</p>

    <!-- Foto's -->
    <h1 class="w3-xxxlarge w3-text-green">Foto's</h1>
    <hr class="w3-round">
    <noscript>
      <div class="w3-panel w3-red w3-center">
      <h3>Javascript is uitgeschakeld!</h3>
      <p>Schakel Javascript in om deze foto's te vergroten.</p>
    </div>
    </noscript>
    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan4.jpg" alt="Foto 4" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan5.jpg" alt="Foto 5" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan6.jpg" alt="Foto 6" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan7.jpg" alt="Foto 7" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan8.jpg" alt="Foto 8" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan9.jpg" alt="Foto 9" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan10.jpg" alt="Foto 10" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan11.jpg" alt="Foto 11" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan12.jpg" alt="Foto 12" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan13.jpg" alt="Foto 13" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan14.jpg" alt="Foto 14" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/modulebaan15.jpg" alt="Foto 15" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
