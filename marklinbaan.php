<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>M&auml;rklinbaan</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Sfeerbeelden -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Märklinbaan</h1>
    <noscript>
      <div class="w3-panel w3-red w3-center">
        <h3>Javascript is uitgeschakeld!</h3>
        <p>Schakel Javascript in om deze foto's te vergroten.</p>
      </div>
    </noscript>
    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan1.jpg" alt="Foto 1" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan2.jpg" alt="Foto 2" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan3.jpg" alt="Foto 3" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>
  </div>

  <div id="modal" class="w3-modal w3-center zoom-out" onclick="this.style.display='none'">
    <img class="w3-modal-content w3-round modal_responsive" id="modal_img">
  </div>

  <script type="text/javascript" language="javascript" src="js/modal.js"></script>

  <!-- Content -->
  <div class="w3-container content">
    <h1 class="w3-xxxlarge w3-text-green">Hoe het begon</h1>
    <hr class="w3-round">
    <p>In 2001 groeide het idee om een Märklinbaan te bouwen, prompt werden er snode plannen gesmeed om dit project te verwezenlijken. Enkele vereisten stonden reeds vast. Het zou een modulaire baan worden, zonder station, maar wel met een steengroeve (de firma die de steengroeve uitbaat is GOMECO, verwijzend naar de twee eerste letters van de familienaam van de bezielers) en laadbunker als blikvanger. Aan de achterzijde dienden opstelsporen te komen in de vorm van opeengestapelde "hondenknoken". Er volgde een tijd van plannen maken, plannen weggooien, opnieuw tekenen en veel fantaseren en dromen.</p>
    <p>Door de positieve inbreng van verschillende andere clubleden moesten er regelmatig wijzigingen aangebracht worden aan het plan. Uiteindelijk werd, in mei van dat jaar, met de werkzaamheden gestart. We moesten tegen de expo van november toch iets kunnen tonen aan de bezoekers. Tegen de opendeur bestond onze baan uit elf naakte modules waar er twee sporen van 3,60 meter op de voorste modules geplaatst waren. We konden er met een locomotief heen en weer rijden. En fier dat we waren toen er een gigantische "Big Boy" de stabiliteit van onze sporen testte.</p>
    <p>Met de volgende opendeur van 2003 waren de zelfgebouwde bruggen geplaatst en konden we al rondjes rijden. Langsheen de straat was er reeds een muur in plasticard gebouwd. De imitatie natuurstenen werden uitgefreest met een Dremeltje en daarna één voor één van een realistische kleur voorzien.</p>
    <p>In 2004 hebben we de achterste modules van opstelsporen voorzien. Op het voorste deel werden de tunnels gebouwd, evenals de drie opstelsporen voor de laadbunker en een deel van de rotswand en het langsheen lopend riviertje. Een gedeelte van de elektrische bediening was ook al geplaatst. Een hele vordering dus voor de nakende opendeurdagen van De Pijl.</p>
    <p>In 2005 werden de rails op de voorzijde bekiezeld en de tunnels afgewerkt. De baan is voorzien van ca. 58 meter spoor en 26 wissels. Ook de elektronica werd voor een deel aangepast. Intussen zijn er enkele nieuwe (en enthousiaste) Märkilnisten onze rangen komen vervoegen.</p>
    <p>De steengroeve kreeg in 2006 vorm en kleur, alsook de berg aan de linkerzijde. Deze berg is zodanig gebouwd dat het bovenste gedeelte afneembaar is voor een eventueel transport. Dit jaar werd er ook aan de elektronica gesleuteld zodat we zowel analoog als digitaal konden rijden. De voorste opstelsporen konden bediend worden met een synoptisch bord.</p>
    <p>Het jaar daarop werd een voorlopige laadbunker in karton gebouwd en het landschap boven de twee rechter tunnels is dan een weide geworden. De Preiser koeien konden dan ook schuilen in een zelfgebouwde houten stal uit plasticard.</p>
    <p>In 2008 is de steengroeve van begroeiing voorzien, de linkse berg is ook van uitzicht veranderd. Er werd een beekje gemaakt met een bruggetje eroverheen. Dat bruggetje was noodzakelijk want de taverne met parking en een speeltuin moest natuurlijk bereikbaar zijn met de wagen. Om dit tafereeltje van het nodige groen te voorzien werden een 70 tal zelfgemaakte dennenbomen gemaakt en geplant. Intussen zijn de werkzaamheden om de definitieve laadbunker te bouwen gestart. Deze wordt volledig in plasticard gemaakt en van werkende transportbanden voorzien.</p>

    <!-- Foto's -->
    <h1 class="w3-xxxlarge w3-text-green">Foto's</h1>
    <hr class="w3-round">
    <noscript>
      <div class="w3-panel w3-red w3-center">
      <h3>Javascript is uitgeschakeld!</h3>
      <p>Schakel Javascript in om deze foto's te vergroten.</p>
    </div>
    </noscript>
    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan4.jpg" alt="Foto 4" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan5.jpg" alt="Foto 5" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan6.jpg" alt="Foto 6" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan7.jpg" alt="Foto 7" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan8.jpg" alt="Foto 8" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan9.jpg" alt="Foto 9" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan10.jpg" alt="Foto 10" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan11.jpg" alt="Foto 11" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan12.jpg" alt="Foto 12" class="width_responsive zoom-in" onclick="openModal(this)"></div>
    </div>

    <!--<div class="w3-row">
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan13.jpg" alt="Foto 13" class="width_responsive zoom-in" onclick="openModal(this)"></div>
      <div class="w3-col l4 m5 s12 w3-center w3-hover-opacity"><img src="images/marklinbaan14.jpg" alt="Foto 14" class="width_responsive zoom-in" onclick="openModal(this)"></div>
  </div> -->
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
