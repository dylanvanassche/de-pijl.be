<!-- Sidenav/menu -->
<nav class="w3-sidenav w3-green w3-collapse w3-top w3-large w3-padding nav_desktop">
  <div class="w3-container w3-padding-64">
    <img src="images/logo.jpg" alt="logo De Pijl" class="w3-round width_responsive">
  </div>
  <a href="index.php" class="w3-padding w3-hover-white">Home</a>
  <a href="marklinbaan.php" class="w3-padding-medium w3-green w3-hover-white">M&auml;rklinbaan</a>
  <a href="modulebaan.php" class="w3-padding-medium w3-green w3-hover-white">Modulebaan</a>
  <a href="clubbaan.php" class="w3-padding-medium w3-green w3-hover-white">Clubbaan</a>
  <a href="bibliotheek.php" class="w3-padding-medium w3-green w3-hover-white">Bibliotheek</a>
  <a href="index.php#locatie" class="w3-padding w3-hover-white">Locatie</a>
  <a href="index.php#lid" class="w3-padding w3-hover-white">Lid worden</a>
  <a href="index.php#contact" class="w3-padding w3-hover-white">Contact</a>
  <a href="disclaimer.php#privacyverklaring" class="w3-padding w3-hover-white">Privacyverklaring</a>
  <a href="downloads.php" class="w3-padding w3-hover-white">Downloads</a>
</nav>
