function openModal(element) {
  var windowsize = window.matchMedia("(min-width: 601px)");
  
  /*Trigger modal not on small screens*/
  if (windowsize.matches) {
    document.getElementById("modal_img").src = element.src;
    document.getElementById("modal").style.display = "block";
  }
}
