<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>MSC De Pijl</span>
</header>


<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Home -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Modelspoorclub De Pijl</h1>
    <h1 class="w3-xxxlarge w3-text-green">Home</h1>
    <hr class="w3-round">
    <p>Welkom op de website van MSC De Pijl VZW, de modelspoorclub in de regio van Vilvoorde en omstreken! Wij houden ons voornamelijk bezig met gelijkstroom en wisselstroom (M&auml;rklin) in een modulair concept. MSC De Pijl is toegankelijk voor jong en oud en gericht op amusement, creativiteit en handigheid, maar ook verbeelding en fantasie.</p>
    <p>Er zijn wekelijks twee clubavonden voorzien op dinsdag- en vrijdagavond vanaf 20 uur. Onze clubavonden gaan door in het Tuchthuis te Vilvoorde (België), voor meer informatie over onze lokalen klik <a href=#locatie>hier</a>.</p>
    <p>De volledige kroniek van MSC De Pijl kan worden gedownload in PDF formaat in de rubiek &#0147;<a href=downloads.html>Downloads</a>&#0148;.</p>
  </div>

  <!-- Banen -->
  <div class="w3-container content" id="banen">
    <h1 class="w3-xxxlarge w3-text-green">Banen</h1>
    <hr class="w3-round">
    <p>In onze lokalen hebben we 2 Belgische banen ter onze beschikking om zowel 2-rail DC als 3-rail AC te kunnen rijden.</p>
  </div>

  <div class="w3-row-padding">
    <div class="w3-half w3-margin-bottom">
      <ul class="w3-ul w3-light-grey w3-center">
        <li class="w3-dark-grey w3-xlarge w3-padding-32">Modulebaan</li>
        <li class="w3-padding-16">PECO-rails</li>
        <li class="w3-padding-16">50+ modules</li>
        <li class="w3-padding-16">Belgisch thema</li>
        <li class="w3-padding-16">Analoog geautomatiseerd</li>
        <li class="w3-padding-16">Digitaal rijden ZIMO DCC</li>
        <li class="w3-light-grey w3-padding-24">
          <a class="w3-btn w3-dark-grey w3-padding-large w3-hover-black" href="modulebaan.html">Meer informatie</a>
        </li>
      </ul>
    </div>

    <div class="w3-half">
      <ul class="w3-ul w3-light-grey w3-center">
        <li class="w3-green w3-xlarge w3-padding-32">M&auml;rklinbaan</li>
        <li class="w3-padding-16">M&auml;rklin K-rails</li>
        <li class="w3-padding-16">20+ modules</li>
        <li class="w3-padding-16">Belgisch thema</li>
        <li class="w3-padding-16">Analoge en/of digitale bediening</li>
        <li class="w3-padding-16">Digitaal rijden MFX/Railcom/DCC/MM</li>
        <li class="w3-light-grey w3-padding-24">
          <a class="w3-btn w3-green w3-padding-large w3-hover-black" href="marklinbaan.html">Meer informatie</a>
        </li>
      </ul>
    </div>
  </div>

  <!-- Locatie -->
  <div class="w3-container content" id="locatie">
    <h1 class="w3-xxxlarge w3-text-green">Locatie en openingsuren</h1>
    <hr class="w3-round">
<iframe id="map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=4.419722557067872%2C50.924137892681685%2C4.42326307296753%2C50.925657831669774&amp;layer=mapnik&amp;marker=50.92489786838309%2C4.4214928150177" style="border: 1px solid black"></iframe>
<a href="https://www.openstreetmap.org/?mlat=50.92490&amp;mlon=4.42149#map=19/50.92490/4.42149">Toon een grotere kaart</a>
    <!--Use Javascript to show the map when Javascript is enabled, otherwise show a warning-->
    <script>
      document.getElementById("map").style.display = "block";
    </script>
    <noscript>
      <div class="w3-panel w3-red w3-center">
        <h3>Javascript is uitgeschakeld!</h3>
        <p>Schakel Javascript in om deze interactieve map te zien.</p>
      </div>
    </noscript>
    <div class="w3-row-padding">
      <div class="w3-half w3-margin-bottom">
        <p>De lokalen van MSC De Pijl zijn gevestigd in het Tuchthuis te Vilvoorde, België.</p>
        <address class="w3-margin">
          MSC De Pijl<br>
          Rondeweg 3/2<br>
          1800 Vilvoorde<br>
          België
        </address>
        <p>Voor alle briefwisseling kan u MSC De Pijl bereiken op:</p>
        <address class="w3-margin">
          MSC De Pijl Secretariaat<br>
          Meiklokjesstraat 12<br>
          1800 Vilvoorde<br>
          België
        </address>

        <p>MSC De Pijl is geopend elke dinsdagavond en vrijdagavond telkens vanaf 20 uur.</p>
      </div>

      <div class="w3-third w3-margin">
        <img src="images/ingangParkingLodge.jpg" alt="Ingang MSC De Pijl" class="w3-round w3-image">
        <p class="w3-center">Ingang MSC De Pijl vanaf de ruime parking</p>
      </div>
    </div>

  </div>

  <!-- Lid worden -->
  <div class="w3-container content" id="lid">
    <h1 class="w3-xxxlarge w3-text-green">Hoe word ik lid?</h1>
    <hr class="w3-round">
    <h2 class="w3-xxlarge w3-text-green">Lid</h2>
    <p>Lid worden kan zeer simpel door een e-mail te sturen of gewoon langs te komen tijdens één van onze clubavonden. Het lidgeld bedraagt 100€/jaar waarmee je  toegang hebt tot de clubavonden, evenementen van MSC De Pijl, de uitgebreide bibliotheek, ...</p>
    <h2 class="w3-xxlarge w3-text-green">VZW-lid</h2>
    <p>Eens je lid bent van MSC De Pijl kan je met een simpele aanvraag VZW-lid worden waarbij je dan stemrecht hebt bij de vergadering.</p>
  </div>

  <!-- Contact -->
  <div class="w3-container content" id="contact">
    <h1 class="w3-xxxlarge w3-text-green">Contact</h1>
    <hr class="w3-round">
    <p id="email">Contacteer MSC De Pijl voor meer informatie via e-mail: <script type="text/javascript" src="js/email.js"></script></p>
        <!--Use Javascript to show the map & my email when Javascript is enabled-->
      <script>
        document.getElementById("email").style.display = "block";
      </script>
      <noscript>
        <div class="w3-panel w3-red w3-center">
          <h3>Javascript is uitgeschakeld!</h3>
          <p>Schakel Javascript in om MSC De Pijl te kunnen contacteren.</p>
        </div>
      </noscript>
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
