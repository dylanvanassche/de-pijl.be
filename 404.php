<!DOCTYPE html>
<html>
<?php include("meta.php")?>
<body>

<!--Menu-->
<?php include("menu_desktop.php")?>

<header class="w3-container w3-top w3-hide-large w3-green w3-xlarge w3-padding">
  <?php include("menu_mobile.php")?>
  <span>Pagina niet gevonden</span>
</header>

<!-- !PAGE CONTENT! -->
<section class="w3-main main">

  <!-- Home -->
  <div class="w3-container content">
    <h1 class="w3-jumbo w3-hide-medium w3-hide-small w3-center">Pagina niet gevonden</h1>
    <p>De gevraagde pagina kan niet worden teruggevonden. Kies een andere pagina in het menu.</p>
  </div>

<!-- End page content -->
</section>

<!--Footer-->
<?php include("footer.php")?>
</body>
</html>
