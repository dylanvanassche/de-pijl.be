<!-- Top menu on small screens -->
<div class="w3-dropdown-hover w3-green">
  <button class="w3-btn w3-green w3-border w3-border-white w3-margin-right w3-round">&#9776;</button>
  <div class="w3-dropdown-content w3-border nav_mobile w3-margin-right">
    <a href="index.php" class="w3-padding w3-green w3-hover-dark-grey">Home</a>
    <a href="marklinbaan.php" class="w3-padding w3-green w3-hover-dark-grey">M&auml;rklinbaan</a>
    <a href="modulebaan.php" class="w3-padding w3-green w3-hover-dark-grey">Modulebaan</a>
    <a href="clubbaan.php" class="w3-padding w3-green w3-hover-dark-grey">Clubbaan</a>
    <a href="bibliotheek.php" class="w3-padding w3-green w3-hover-dark-grey">Bibliotheek</a>
    <a href="index.php#locatie" class="w3-padding w3-green w3-hover-dark-grey">Locatie</a>
    <a href="index.php#lid" class="w3-padding w3-green w3-hover-dark-grey">Lid worden</a>
    <a href="index.php#contact" class="w3-padding w3-green w3-hover-dark-grey">Contact</a>
    <a href="disclaimer.php#privacyverklaring" class="w3-padding w3-green w3-hover-dark-grey">Privacyverklaring</a>
    <a href="downloads.php" class="w3-padding w3-green w3-hover-dark-grey">Downloads</a>
  </div>
</div>
